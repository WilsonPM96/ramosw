//
//  ArrayInfo.swift
//  ExamenramosW
//
//  Created by Wilson Gabriel Ramos Bravo on 5/6/18.
//  Copyright © 2018 Wilson Gabriel Ramos Bravo. All rights reserved.
//

import Foundation
struct ArrayExamInfo:Decodable {
    let data: [ArrayInfo]
}

struct ArrayInfo:Decodable {
    let label:String
    let value:Int //entero
}
