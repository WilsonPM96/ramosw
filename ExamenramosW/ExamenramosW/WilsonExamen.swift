//
//  WilsonExamen.swift
//  ExamenramosW
//
//  Created by Wilson Gabriel Ramos Bravo on 5/6/18.
//  Copyright © 2018 Wilson Gabriel Ramos Bravo. All rights reserved.
//

import Foundation
struct WilsonExamInfo:Decodable {
    let viewTitle: String
    let date: String
    let nextLink: String
    
}

