//
//  ViewController.swift
//  ExamenramosW
//
//  Created by Wilson Gabriel Ramos Bravo on 5/6/18.
//  Copyright © 2018 Wilson Gabriel Ramos Bravo. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var viewTitleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var nameTextFielld: UITextField!
    @IBAction func nextButton(_ sender: Any) {
       
         performSegue(withIdentifier: "ExamenViewController", sender: self)
    }
    
    var newUrl = "https://api.myjson.com/bins/182sje"
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ExamenViewController"{
            let destination = segue.destination as! InfoExamViewController
            destination.nombre = nameTextFielld.text!
            
        }
     
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        let urlString = "https://api.myjson.com/bins/72936"
        let url = URL(string: urlString)
        
        let session = URLSession.shared
        
        
        let task = session.dataTask(with: url!){(data, response, error) in
            guard let data = data else {  //nos aseguramos de que exista la data que estamos tomando
                print ("Error No Data")
                return
            }
            guard let appInfo = try? JSONDecoder().decode(WilsonExamInfo.self, from: data)
                else {
                    print("Error decoding Exam info")
                    return
            }
            DispatchQueue.main.async {
                self.viewTitleLabel.text = "\(appInfo.viewTitle)"
                self.dateLabel.text = "\(appInfo.date)"
                self.newUrl = "\(appInfo.nextLink)"
            
                
            }
            
            
        }
        task.resume()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    


}

