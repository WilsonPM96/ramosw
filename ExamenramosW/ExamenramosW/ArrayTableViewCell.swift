//
//  ArrayTableViewCell.swift
//  ExamenramosW
//
//  Created by Wilson Gabriel Ramos Bravo on 6/6/18.
//  Copyright © 2018 Wilson Gabriel Ramos Bravo. All rights reserved.
//

import UIKit

class ArrayTableViewCell: UITableViewCell {

    @IBOutlet weak var labelTableViewLabel: UILabel!
    @IBOutlet weak var valueTableViewLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
