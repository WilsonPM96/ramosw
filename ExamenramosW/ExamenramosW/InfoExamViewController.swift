//
//  InfoExamViewController.swift
//  ExamenramosW
//
//  Created by Wilson Gabriel Ramos Bravo on 5/6/18.
//  Copyright © 2018 Wilson Gabriel Ramos Bravo. All rights reserved.
//

import UIKit

class InfoExamViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var arrayTableView: UITableView!
   
    @IBOutlet weak var averageLabel: UILabel!
    
   
    let viewModel = ViewController()
    var nombre: String = ""
    var arrayofItems: [String] = []
    var numbers: [Int] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        nameLabel?.text = "Hi, "+nombre
        let urlString = "https://api.myjson.com/bins/182sje"
        let url = URL(string: urlString)

        let session = URLSession.shared

        let task = session.dataTask(with: url!){(data, response, error) in
            guard let data = data else {  //nos aseguramos de que exista la data que estamos tomando
                print ("Error No Data")
                return
            }
            guard let appInfo = try? JSONDecoder().decode(ArrayExamInfo.self, from: data)
                else {
                    print("Error decoding Array Exam info")
                    return
            }
            DispatchQueue.main.async {
                self.arrayofItems.append("\(appInfo.data[0].label)")
                self.arrayofItems.append("\(appInfo.data[1].label)")
                self.arrayofItems.append("\(appInfo.data[2].label)")
                self.arrayofItems.append("\(appInfo.data[3].label)")
                self.numbers.append(appInfo.data[0].value)
                self.numbers.append(appInfo.data[1].value)
                self.numbers.append(appInfo.data[2].value)
                self.numbers.append(appInfo.data[3].value)
                
                let uno: Int = appInfo.data[0].value
                let dos: Int = appInfo.data[1].value
                let tres: Int = appInfo.data[2].value
                let cuatro: Int = appInfo.data[3].value
                let suma: Int = (uno + dos + tres + cuatro)
                let cte: Int = 4
                let promedio: Decimal = Decimal(suma/cte)
                self.averageLabel.text = "\(promedio)"
                self.arrayTableView.reloadData()
                

            }

        }
        task.resume()
        
    }
        // Do any additional setup after loading the view.
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      return arrayofItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "mycell", for: indexPath) as! ArrayTableViewCell
        cell.labelTableViewLabel.text = arrayofItems[indexPath.row]
        cell.valueTableViewLabel.text = "\(numbers[indexPath.row])"
        return cell
        
    
    }
    }
    
  
    

    



